## Tools

This is my collection of standalone tools i use on a regular basis for IT-Security / Pentesting purposes.

To clone this repository run:

```bash
git clone --recursive -j8 git://gitlab.com/MagicBytes/tools.git
```
